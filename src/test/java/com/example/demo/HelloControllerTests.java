package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noParam_rtnHelloWorld() throws Exception {
        // /hello string "Hello World"
        // Arrange
        // Act
        mockMvc.perform(get("/hello"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));
    }

    @Test
    void sayHello_myName_rtnHelloName() throws Exception {
        mockMvc.perform(get("/hello?name=Hector"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Hector"));
    }

    @Test
    void showCalculate_noParam_rtnCalculate() throws Exception {
        mockMvc.perform(get("/calculate"))
                .andExpect(status().isOk())
                .andExpect(content().string("The Result is: 0"));
    }

    @Test
    void canSumTwoNumbers() throws Exception {
        mockMvc.perform(get("/calculate?fnumber=9&operator=+&snumber=2"))
                .andExpect(status().isOk())
                .andExpect(content().string("The Result is: 11"));
    }
    @Test
    void canSubtractTwoNumbers() throws Exception {
        mockMvc.perform(get("/calculate?fnumber=9&operator=-&snumber=2"))
                .andExpect(status().isOk())
                .andExpect(content().string("The Result is: 7"));
    }
    @Test
    void canDivideTwoNumbers() throws Exception {
        mockMvc.perform(get("/calculate?fnumber=9&operator=/&snumber=3"))
                .andExpect(status().isOk())
                .andExpect(content().string("The Result is: 3"));
    }
    @Test
    void canMultiplyTwoNumbers() throws Exception {
        mockMvc.perform(get("/calculate?fnumber=9&operator=*&snumber=2"))
                .andExpect(status().isOk())
                .andExpect(content().string("The Result is: 18"));
    }

    @Test
    void canCountTheVowels_retTheValue() throws Exception {
        mockMvc.perform(get("/vowels?word=bcdf"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    void canCountMoreThanOneVowel_retNumberOfVowels() throws Exception {
        mockMvc.perform(get("/vowels?word=abca"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));
    }
    @Test
    void canCountDifferentVowels_retNumberOfVowels() throws Exception {
        mockMvc.perform(get("/vowels?word=abcaoue"))
                .andExpect(status().isOk())
                .andExpect(content().string("5"));
    }
 }

