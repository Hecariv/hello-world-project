package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(@RequestParam(defaultValue = "World", required = false) String name) {
        return String.format("Hello %s", name);
    }

    @GetMapping("/calculate")
    public String calculate(@RequestParam(defaultValue = "0", required = false) int fnumber,
                            @RequestParam(defaultValue = "0", required = false) int snumber,
                            @RequestParam(defaultValue = "+", required = false) String operator) {
        switch (operator) {
            case "+":
                return String.format("The Result is: %d", (fnumber + snumber));
            case "-":
                return String.format("The Result is: %d", (fnumber - snumber));
            case "*":
                return String.format("The Result is: %d", (fnumber * snumber));
            case "/":
                return String.format("The Result is: %d", (fnumber / snumber));
        }
        return "The Result is: 0";
    }

    @GetMapping("/vowels")
    public String countVowels(@RequestParam(defaultValue = "", required = false) String word) {
        int tmp = 0;
        for (int i = 0; i < word.length(); i++) {
            char currentLetter = word.charAt(i);
            if (currentLetter == 'a' || currentLetter == 'e' ||
                currentLetter == 'i' || currentLetter == 'o' ||
                currentLetter == 'u') {
                tmp++;
            }
        }
        if (tmp > 0) {
            return String.valueOf(tmp);
        } else {
            return "0";
        }
    }


}